<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/04/16
 * Time: 16:31
 */

namespace eezeecommerce\SettingsBundle\Tests\Provider;

use Doctrine\Common\Persistence\ObjectRepository;
use eezeecommerce\SettingsBundle\Entity\Settings;
use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use Doctrine\ORM\EntityManager;

class SettingsProviderTest extends \PHPUnit_Framework_TestCase
{

    public function testConstructorAcceptsEntityManager()
    {
        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        new SettingsProvider($em);
    }

    public function testFindAllWithIdOf2ReturnsSetting()
    {
        $repository = $this->getMockBuilder(ObjectRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $setting1 = $this->getMockBuilder(Settings::class)
            ->disableOriginalConstructor()
            ->getMock();
        $setting1->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(2));

        $em->expects($this->once())
            ->method("getRepository")
            ->will($this->returnValue($repository));

        $repository->expects($this->once())
            ->method("findAll")
            ->will($this->returnValue(array($setting1)));

        $provider = new SettingsProvider($em);

        $result = $provider->loadSettingsBySite();

        $this->assertTrue(!is_array($result));

        $this->assertEquals($setting1, $result);

        $this->assertEquals(2, $result->getId());
    }
}
