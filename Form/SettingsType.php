<?php

namespace eezeecommerce\SettingsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('store_name')
            ->add('legal_name')
            ->add('company_number')
            ->add('vat_number')
            ->add('business_contact_email')
            ->add('business_contact_phone')
            ->add('business_url')
            ->add('business_address_1')
            ->add('business_address_2')
            ->add('business_address_city')
            ->add('business_address_postcode')
            ->add('business_address_county')
            ->add(
                'business_address_country',
                'entity',
                [
                    'class' => "eezeecommerceShippingBundle:Country",
                    'property' => "name",
                    "multiple" => false,
                    "label" => false,
                ]
            )
            ->add('order_prefix')
            ->add('order_suffix')
            ->add('category_show_empty', null, ["label" => "Show categories when empty"])
            ->add('cart_enabled', null, ["label" => "Show 'My Cart' on navbar"])
            ->add(
                'default_currency',
                'entity',
                [
                    'class' => "eezeecommerceCurrencyBundle:Currency",
                    'property' => "currency_code",
                    "multiple" => false,
                    "label" => false,
                ]
            )
            ->add(
                "min_turnaround_time",
                null,
                [
                    "label" => "Set default global min turnaround",
                ]
            )
            ->add(
                "max_turnaround_time",
                null,
                [
                    "label" => "Set default global max turnaround",
                ]
            )
            ->add(
                'default',
                null,
                [
                    "label" => false,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'eezeecommerce\SettingsBundle\Entity\Settings',
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_settingsbundle_settings';
    }

}
