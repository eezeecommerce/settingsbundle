<?php

namespace eezeecommerce\SettingsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TaxesSettingsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('inclusive', null, array(
                "label" => "Products include VAT",
            ))
            ->add('shipping_inclusive', null, array(
                "label" => "Shipping rates include VAT",
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\SettingsBundle\Entity\TaxesSettings'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_settingsbundle_taxessettings';
    }
}
