<?php

namespace eezeecommerce\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('eezeecommerceSettingsBundle:Default:index.html.twig', array('name' => $name));
    }
}
