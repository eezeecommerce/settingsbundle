<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\SettingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="tax_settings")
 * @ORM\Entity(repositoryClass="eezeecommerce\SettingsBundle\Entity\TaxesSettingsRepository")
 */
class TaxesSettings
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * 
     * @var integer $id
     */
    protected $id;
            
    /**
     * @ORM\OneToOne(targetEntity="eezeecommerce\SettingsBundle\Entity\Settings", inversedBy="tax_settings")
     * @ORM\JoinColumn(name="settings_id", referencedColumnName="id")
     */
    protected $settings;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $inclusive = false;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $shipping_inclusive = false;
    
    /**
     * @ORM\OneToMany(targetEntity="eezeecommerce\TaxBundle\Entity\TaxRates", mappedBy="tax_settings")
     */
    protected $tax_rates;
    
    
    public function __construct()
    {
        $this->tax_rates = new ArrayCollection();
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inclusive
     *
     * @param boolean $inclusive
     *
     * @return TaxesSettings
     */
    public function setInclusive($inclusive)
    {
        $this->inclusive = $inclusive;

        return $this;
    }

    /**
     * Get inclusive
     *
     * @return boolean
     */
    public function getInclusive()
    {
        return $this->inclusive;
    }

    /**
     * Set shippingInclusive
     *
     * @param boolean $shippingInclusive
     *
     * @return TaxesSettings
     */
    public function setShippingInclusive($shippingInclusive)
    {
        $this->shipping_inclusive = $shippingInclusive;

        return $this;
    }

    /**
     * Get shippingInclusive
     *
     * @return boolean
     */
    public function getShippingInclusive()
    {
        return $this->shipping_inclusive;
    }

    /**
     * Set settings
     *
     * @param \eezeecommerce\SettingsBundle\Entity\Settings $settings
     *
     * @return TaxesSettings
     */
    public function setSettings(\eezeecommerce\SettingsBundle\Entity\Settings $settings = null)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Get settings
     *
     * @return \eezeecommerce\SettingsBundle\Entity\Settings
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Add taxRate
     *
     * @param \eezeecommerce\TaxBundle\Entity\TaxRates $taxRate
     *
     * @return TaxesSettings
     */
    public function addTaxRate(\eezeecommerce\TaxBundle\Entity\TaxRates $taxRate)
    {
        $this->tax_rates[] = $taxRate;

        return $this;
    }

    /**
     * Remove taxRate
     *
     * @param \eezeecommerce\TaxBundle\Entity\TaxRates $taxRate
     */
    public function removeTaxRate(\eezeecommerce\TaxBundle\Entity\TaxRates $taxRate)
    {
        $this->tax_rates->removeElement($taxRate);
    }

    /**
     * Get taxRates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaxRates()
    {
        return $this->tax_rates;
    }
}
