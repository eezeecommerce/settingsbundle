<?php

namespace eezeecommerce\SettingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="settings")
 * @ORM\Entity(repositoryClass="eezeecommerce\SettingsBundle\Entity\SettingsRepository")
 */
class Settings
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * 
     * @var integer $id
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $store_name;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $legal_name;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $company_number;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $vat_number;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $business_contact_email;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $business_contact_phone;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $business_url;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $business_address_1;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $business_address_2;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $business_address_city;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $business_address_postcode;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $business_address_county;
    
    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\ShippingBundle\Entity\Country", inversedBy="settings")
     */
    protected $business_address_country;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $order_prefix;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $order_suffix;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $category_show_empty = false;
    
    /**
     * @ORM\OneToOne(targetEntity="eezeecommerce\SettingsBundle\Entity\TaxesSettings", mappedBy="settings")
     */
    protected $tax_settings;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $min_turnaround_time = 1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $max_turnaround_time = 1;
    
    /**
     * @ORM\OneToMany(targetEntity="eezeecommerce\CurrencyBundle\Entity\Currency", mappedBy="settings", fetch="LAZY")
     */
    protected $currencies;

    /**
     * @ORM\Column(type="string", length=90)
     */
    protected $host;
    
    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\CurrencyBundle\Entity\Currency", inversedBy="default_currencies")
     * @ORM\JoinColumn(name="default_currency_id", referencedColumnName="id")
     */
    protected $default_currency;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $default;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $cart_enabled = 1;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->currencies = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set storeName
     *
     * @param string $storeName
     *
     * @return Settings
     */
    public function setStoreName($storeName)
    {
        $this->store_name = $storeName;

        return $this;
    }

    /**
     * Get storeName
     *
     * @return string
     */
    public function getStoreName()
    {
        return $this->store_name;
    }

    /**
     * Set legalName
     *
     * @param string $legalName
     *
     * @return Settings
     */
    public function setLegalName($legalName)
    {
        $this->legal_name = $legalName;

        return $this;
    }

    /**
     * Get legalName
     *
     * @return string
     */
    public function getLegalName()
    {
        return $this->legal_name;
    }

    /**
     * Set companyNumber
     *
     * @param string $companyNumber
     *
     * @return Settings
     */
    public function setCompanyNumber($companyNumber)
    {
        $this->company_number = $companyNumber;

        return $this;
    }

    /**
     * Get companyNumber
     *
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->company_number;
    }

    /**
     * Set vatNumber
     *
     * @param string $vatNumber
     *
     * @return Settings
     */
    public function setVatNumber($vatNumber)
    {
        $this->vat_number = $vatNumber;

        return $this;
    }

    /**
     * Get vatNumber
     *
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vat_number;
    }

    /**
     * Set businessContactEmail
     *
     * @param string $businessContactEmail
     *
     * @return Settings
     */
    public function setBusinessContactEmail($businessContactEmail)
    {
        $this->business_contact_email = $businessContactEmail;

        return $this;
    }

    /**
     * Get businessContactEmail
     *
     * @return string
     */
    public function getBusinessContactEmail()
    {
        return $this->business_contact_email;
    }

    /**
     * Set businessContactPhone
     *
     * @param string $businessContactPhone
     *
     * @return Settings
     */
    public function setBusinessContactPhone($businessContactPhone)
    {
        $this->business_contact_phone = $businessContactPhone;

        return $this;
    }

    /**
     * Get businessContactPhone
     *
     * @return string
     */
    public function getBusinessContactPhone()
    {
        return $this->business_contact_phone;
    }

    /**
     * Set businessAddress1
     *
     * @param string $businessAddress1
     *
     * @return Settings
     */
    public function setBusinessAddress1($businessAddress1)
    {
        $this->business_address_1 = $businessAddress1;

        return $this;
    }

    /**
     * Get businessAddress1
     *
     * @return string
     */
    public function getBusinessAddress1()
    {
        return $this->business_address_1;
    }

    /**
     * Set businessAddress2
     *
     * @param string $businessAddress2
     *
     * @return Settings
     */
    public function setBusinessAddress2($businessAddress2)
    {
        $this->business_address_2 = $businessAddress2;

        return $this;
    }

    /**
     * Get businessAddress2
     *
     * @return string
     */
    public function getBusinessAddress2()
    {
        return $this->business_address_2;
    }

    /**
     * Set businessAddressCity
     *
     * @param string $businessAddressCity
     *
     * @return Settings
     */
    public function setBusinessAddressCity($businessAddressCity)
    {
        $this->business_address_city = $businessAddressCity;

        return $this;
    }

    /**
     * Get businessAddressCity
     *
     * @return string
     */
    public function getBusinessAddressCity()
    {
        return $this->business_address_city;
    }

    /**
     * Set businessAddressPostcode
     *
     * @param string $businessAddressPostcode
     *
     * @return Settings
     */
    public function setBusinessAddressPostcode($businessAddressPostcode)
    {
        $this->business_address_postcode = $businessAddressPostcode;

        return $this;
    }

    /**
     * Get businessAddressPostcode
     *
     * @return string
     */
    public function getBusinessAddressPostcode()
    {
        return $this->business_address_postcode;
    }

    /**
     * Set businessAddressCounty
     *
     * @param string $businessAddressCounty
     *
     * @return Settings
     */
    public function setBusinessAddressCounty($businessAddressCounty)
    {
        $this->business_address_county = $businessAddressCounty;

        return $this;
    }

    /**
     * Get businessAddressCounty
     *
     * @return string
     */
    public function getBusinessAddressCounty()
    {
        return $this->business_address_county;
    }

    /**
     * Set orderPrefix
     *
     * @param string $orderPrefix
     *
     * @return Settings
     */
    public function setOrderPrefix($orderPrefix)
    {
        $this->order_prefix = $orderPrefix;

        return $this;
    }

    /**
     * Get orderPrefix
     *
     * @return string
     */
    public function getOrderPrefix()
    {
        return $this->order_prefix;
    }

    /**
     * Set orderSuffix
     *
     * @param string $orderSuffix
     *
     * @return Settings
     */
    public function setOrderSuffix($orderSuffix)
    {
        $this->order_suffix = $orderSuffix;

        return $this;
    }

    /**
     * Get orderSuffix
     *
     * @return string
     */
    public function getOrderSuffix()
    {
        return $this->order_suffix;
    }

    /**
     * Set businessUrl
     *
     * @param string $businessUrl
     *
     * @return Settings
     */
    public function setBusinessUrl($businessUrl)
    {
        $this->business_url = $businessUrl;

        return $this;
    }

    /**
     * Get businessUrl
     *
     * @return string
     */
    public function getBusinessUrl()
    {
        return $this->business_url;
    }

    /**
     * Set taxSettings
     *
     * @param \eezeecommerce\SettingsBundle\Entity\TaxesSettings $taxSettings
     *
     * @return Settings
     */
    public function setTaxSettings(\eezeecommerce\SettingsBundle\Entity\TaxesSettings $taxSettings = null)
    {
        $this->tax_settings = $taxSettings;

        return $this;
    }

    /**
     * Get taxSettings
     *
     * @return \eezeecommerce\SettingsBundle\Entity\TaxesSettings
     */
    public function getTaxSettings()
    {
        return $this->tax_settings;
    }

    /**
     * Set defaultCurrencyCode
     *
     * @param string $defaultCurrencyCode
     *
     * @return Settings
     */
    public function setDefaultCurrencyCode($defaultCurrencyCode)
    {
        $this->default_currency_code = $defaultCurrencyCode;

        return $this;
    }

    /**
     * Get defaultCurrencyCode
     *
     * @return string
     */
    public function getDefaultCurrencyCode()
    {
        return $this->default_currency_code;
    }

    /**
     * Add currency
     *
     * @param \eezeecommerce\CurrencyBundle\Entity\Currency $currency
     *
     * @return Settings
     */
    public function addCurrency(\eezeecommerce\CurrencyBundle\Entity\Currency $currency)
    {
        $this->currencies[] = $currency;

        return $this;
    }

    /**
     * Remove currency
     *
     * @param \eezeecommerce\CurrencyBundle\Entity\Currency $currency
     */
    public function removeCurrency(\eezeecommerce\CurrencyBundle\Entity\Currency $currency)
    {
        $this->currencies->removeElement($currency);
    }

    /**
     * Get currencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * Set host
     *
     * @param string $host
     *
     * @return Settings
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set categoryShowEmpty
     *
     * @param boolean $categoryShowEmpty
     *
     * @return Settings
     */
    public function setCategoryShowEmpty($categoryShowEmpty)
    {
        $this->category_show_empty = $categoryShowEmpty;

        return $this;
    }

    /**
     * Get categoryShowEmpty
     *
     * @return boolean
     */
    public function getCategoryShowEmpty()
    {
        return $this->category_show_empty;
    }

    /**
     * Set cartEnabled
     *
     * @param boolean $cartEnabled
     *
     * @return Settings
     */
    public function setCartEnabled($cartEnabled)
    {
        $this->cart_enabled = $cartEnabled;

        return $this;
    }

    /**
     * Get cartEnabled
     *
     * @return boolean
     */
    public function getCartEnabled()
    {
        return $this->cart_enabled;
    }

    /**
     * Set default
     *
     * @param boolean $default
     *
     * @return Settings
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return boolean
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set minTurnaroundTime
     *
     * @param integer $minTurnaroundTime
     *
     * @return Settings
     */
    public function setMinTurnaroundTime($minTurnaroundTime)
    {
        $this->min_turnaround_time = $minTurnaroundTime;

        return $this;
    }

    /**
     * Get minTurnaroundTime
     *
     * @return integer
     */
    public function getMinTurnaroundTime()
    {
        return $this->min_turnaround_time;
    }

    /**
     * Set maxTurnaroundTime
     *
     * @param integer $maxTurnaroundTime
     *
     * @return Settings
     */
    public function setMaxTurnaroundTime($maxTurnaroundTime)
    {
        $this->max_turnaround_time = $maxTurnaroundTime;

        return $this;
    }

    /**
     * Get maxTurnaroundTime
     *
     * @return integer
     */
    public function getMaxTurnaroundTime()
    {
        return $this->max_turnaround_time;
    }

    /**
     * Get businessAddressCountry
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBusinessAddressCountry()
    {
        return $this->business_address_country;
    }

    /**
     * Add defaultCurrency
     *
     * @param \eezeecommerce\CurrencyBundle\Entity\Currency $defaultCurrency
     *
     * @return Settings
     */
    public function addDefaultCurrency(\eezeecommerce\CurrencyBundle\Entity\Currency $defaultCurrency)
    {
        $this->default_currency[] = $defaultCurrency;

        return $this;
    }

    /**
     * Remove defaultCurrency
     *
     * @param \eezeecommerce\CurrencyBundle\Entity\Currency $defaultCurrency
     */
    public function removeDefaultCurrency(\eezeecommerce\CurrencyBundle\Entity\Currency $defaultCurrency)
    {
        $this->default_currency->removeElement($defaultCurrency);
    }

    /**
     * Set businessAddressCountry
     *
     * @param \eezeecommerce\ShippingBundle\Entity\Country $businessAddressCountry
     *
     * @return Settings
     */
    public function setBusinessAddressCountry(\eezeecommerce\ShippingBundle\Entity\Country $businessAddressCountry = null)
    {
        $this->business_address_country = $businessAddressCountry;

        return $this;
    }

    /**
     * Set defaultCurrency
     *
     * @param \eezeecommerce\CurrencyBundle\Entity\Currency $defaultCurrency
     *
     * @return Settings
     */
    public function setDefaultCurrency(\eezeecommerce\CurrencyBundle\Entity\Currency $defaultCurrency = null)
    {
        $this->default_currency = $defaultCurrency;

        return $this;
    }

    /**
     * Get defaultCurrency
     *
     * @return \eezeecommerce\CurrencyBundle\Entity\Currency
     */
    public function getDefaultCurrency()
    {
        return $this->default_currency;
    }
}
