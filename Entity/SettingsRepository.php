<?php

namespace eezeecommerce\SettingsBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SettingsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SettingsRepository extends EntityRepository
{
    
    public function getSettings()
    {
        $query = $this->getEntityManager()
                ->createQueryBuilder();
        $query->select("s")
                ->from("eezeecommerceSettingsBundle:Settings", "s");
        
        return $query->getQuery()->getSingleResult();
    }

    public function getMenuSettings()
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder();
        $query->select("s.category_show_empty, s.cart_enabled")
            ->from("eezeecommerceSettingsBundle:Settings", "s");

        return $query->getQuery()->getSingleResult();
    }
    
}
