<?php

namespace eezeecommerce\SettingsBundle\Provider;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use eezeecommerce\SettingsBundle\Entity\Settings;

/**
 * Class SettingsProvider
 *
 * @package eezeecommerce\SettingsBundle\Provider
 */
class SettingsProvider
{
    /**
     * Object Manager
     *
     * @var ObjectManager
     */
    protected $em;

    /**
     * SettingsProvider constructor.
     *
     * @param ObjectManager $em ObjectManager
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * Load the site based settings to determine the currency, roles and products
     *
     * @return Settings
     */
    public function loadSettingsBySite()
    {
        $setting = $this->em->getRepository("eezeecommerceSettingsBundle:Settings")
            ->getSettings();

        return $setting;
    }
}